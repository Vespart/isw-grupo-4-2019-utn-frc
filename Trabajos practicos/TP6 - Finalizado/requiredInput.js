var selFormaPago = document.getElementById("selectFormaPago");
var selTiempoRec = document.getElementById("selectTiempoRecepcion");

$('#selectFormaPago').on('change', function(){
    if(selFormaPago.value=="pagoEfectivo"){
        
        document.getElementById("inputMontoEfectivo").required = true;
        document.getElementById("inputNombreApellido").required = false;
        document.getElementById("cc").required = false;
        document.getElementById("inputFechaVencTarjeta").required = false;
        document.getElementById("cvv").required = false;
    }
    else if(selFormaPago.value=="pagoTarjeta"){

        document.getElementById("inputMontoEfectivo").required = false;
        document.getElementById("inputNombreApellido").required = true;
        document.getElementById("cc").required = true;
        document.getElementById("inputFechaVencTarjeta").required = true;
        document.getElementById("cvv").required = true;
    }
})


$('#selectTiempoRecepcion').on('change', function(){
    if(selTiempoRec.value=="antesPosible"){
        document.getElementById("inputFechaRecepcion").required = false;
        document.getElementById("inputHoraRecepcion").required = false;
    }
    else if(selTiempoRec.value=="opcionFechaYHora"){
        document.getElementById("inputFechaRecepcion").required = true;
        document.getElementById("inputHoraRecepcion").required = true;
    }
})

$('#inputFechaVencTarjeta').on('change', function(){
    var j = (new Date()).toISOString().split('T')[0];
    var sdate = document.getElementById('inputFechaVencTarjeta').value;

    if (sdate < j) {
        alert("La fecha ingresada no es valida");
        document.getElementById("inputFechaVencTarjeta").valueAsDate = null;
    }
  });

  $('#inputFechaRecepcion').on('change', function(){
    var j = (new Date()).toISOString().split('T')[0];
    var sdate = document.getElementById('inputFechaRecepcion').value;

    if (sdate < j) {
        alert("La fecha ingresada no es valida");
        document.getElementById("inputFechaRecepcion").valueAsDate = null;
    }
    else{
        $('#inputHoraRecepcion').prop('disabled', false);
    }
  });