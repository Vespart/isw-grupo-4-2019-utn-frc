var miapp = angular.module('miapp', []);

miapp.controller('miController', function ($scope) {
    $scope.verForm = false;
    $scope.confirmado = false;
    $scope.ciudades = [{"nombre" :"Ciudad de Córdoba"},{"nombre" :"Salta"},{"nombre" :"Rosario"}
                        ,{"nombre" :"La Plata"},{"nombre" :"Mar de Plata"},{"nombre" :"San Carlos de Bariloche"}
                        ,{"nombre" :"Neuquen"}];

    $scope.productos = [{"nombre":"Hamburguesa","imagen":"Hamburguesa.png","costo":"220","cantidad":"1"},
                        {"nombre":"Taco","imagen":"Taco.png","costo":"120","cantidad":"2"}]
                        
    $scope.hamburguesa = [{"nombre":"Hamburguesa","imagen":"Hamburguesa.png","costo":"220","cantidad":"1"}]

    $scope.getTotal = function(){
        var total = 0;
        for(var i = 0; i < $scope.productos.length; i++){
            var product = $scope.productos[i];
            total += (product.costo * product.cantidad);
        }
        return total;
    }

    $scope.agregar = function(){
        var product = $scope.hamburguesa[0];
        $scope.productos.push(product);
    }

    $scope.quitar = function(){
        $scope.productos.splice($scope.productos.indexOf($scope.productos), 1);
    }
    $scope.showForm = function(){
        
        if($scope.productos.length != 0){
            $scope.verForm=true;
        }else{
            alert("Debe tener al menos un producto en el carrito para seguir con el pago");
        }
    }
    $scope.closeForm = function(){
        $scope.verForm=false;
    }
    $scope.confirmarPedi = function(){
        if($scope.Formulario.$valid){
            $scope.confirmado=true;
        }
    }

});

(function() {
    'use strict';
    window.addEventListener('load', function() {
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
            
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();


  