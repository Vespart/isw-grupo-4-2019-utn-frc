var miapp = angular.module('miapp', []);

miapp.controller('miController', function ($scope) {
    $scope.verForm = false;
    $scope.variable = "a";
    $scope.confirmacion = "false";
    $scope.confirmado=false;

    $scope.ciudades = [{"nombre" :"Córdoba"},{"nombre" :"Buenos Aires"},{"nombre" :"San Juan"}
                        ,{"nombre" :"Tucuman"},{"nombre" :"Entre Rios"},{"nombre" :"San Luis"}
                        ,{"nombre" :"Panama"},{"nombre" :"Jujuy"},{"nombre" :"Mendoza"}
                        ,{"nombre" :"Formosa"},{"nombre" :"Tierra del fuego"}];

    $scope.productos = [{"nombre":"Hamburguesa","imagen":"Hamburguesa.png","costo":"220","cantidad":"1"},
                        {"nombre":"Taco","imagen":"Taco.png","costo":"120","cantidad":"2"}]


    $scope.hamburguesa = [{"nombre":"Hamburguesa","imagen":"Hamburguesa.png","costo":"220","cantidad":"1"}]

    $scope.getTotal = function(){
        var total = 0;
        for(var i = 0; i < $scope.productos.length; i++){
            var product = $scope.productos[i];
            total += (product.costo * product.cantidad);
        }
        return total;
    }

    $scope.agregar = function(){
        var product = $scope.hamburguesa[0];
        $scope.productos.push(product);
    }

    $scope.quitar = function(){
        $scope.productos.splice($scope.productos.indexOf($scope.productos), 1);
    }

    $scope.showForm = function(){

        if($scope.productos.length != 0){
            $scope.verForm=true;
        }else{
            alert("Debe tener al menos un producto en el carrito para seguir con el pago");
        }
    }


    $scope.closeForm = function(){
        $scope.verForm=false;
    }
    
    $scope.confirmar = function(){
        $scope.confirmacion = "true";
    }

    $scope.confirmarPedi = function(){
        if($scope.Formulario.$valid){
            $scope.confirmado=true;
        }
    }

    $scope.tarjetaOK = function(){
        alert("La tarjeta cargada es correcta!")
    }

    $('#inputFechaVencTarjeta').on('change', function(){
        var j = (new Date()).toISOString().split('T')[0];
        var sdate = document.getElementById('inputFechaVencTarjeta').value;
    
        if (sdate < j) {
            alert("La fecha ingresada no es valida");
            document.getElementById("inputFechaVencTarjeta").valueAsDate = null;
        }
      });
    
    $('#inputFechaRecepcion').on('change', function(){
        var j = (new Date()).toISOString().split('T')[0];
        var sdate = document.getElementById('inputFechaRecepcion').value;

        if (sdate < j) {
            alert("La fecha ingresada no es valida");
            document.getElementById("inputFechaRecepcion").valueAsDate = null;
        }
        else{
            $('#inputHoraRecepcion').prop('disabled', false);
        }
    });


    $('#btnConfirmar').on('click', function(){

        var sdateFecha = document.getElementById('inputFechaRecepcion').value;
        var j = (new Date()).toISOString().split('T')[0];
    
        var sdate = document.getElementById('inputHoraRecepcion').value;
        var horaSelec = sdate.substring(0,2);
        var minSelec = sdate.substring(3,5);
    
        var getHora = new Date();
        var getMin = new Date();
        var minActual = getMin.getMinutes();
        var horaActual = getHora.getHours();
    
        if (sdateFecha == j) {
            if(horaSelec < horaActual){
                alert("Ingreso un horario pasado");
                document.getElementById("inputHoraRecepcion").valueAsDate = null;
            }else if(horaSelec == horaActual){
                if(minSelec < minActual){
                    alert("Ingreso un horario pasado");
                }
            }
        }
    
      });


    var selFormaPago = document.getElementById("selectFormaPago");
    var selTiempoRec = document.getElementById("selectTiempoRecepcion");
    
    $scope.validarCampos = function(){
        if ($scope.calle.$valid){
            alert("123")
        }

    }

    $('#selectFormaPago').on('change', function(){
        if(selFormaPago.value=="pagoEfectivo"){
            /*
            $("#inputMontoEfectivo").attr('required', true);
            $("#inputNombreApellido").removeAttr('required');
            $("#cc").removeAttr('required');
            $("#inputFechaVencTarjeta").removeAttr('required');
            $("#cvv").removeAttr('required');*/

            document.getElementById("inputMontoEfectivo").required = true;

            document.getElementById("inputNombreApellido").required = false;
            document.getElementById("cc").required = false;
            document.getElementById("inputFechaVencTarjeta").required = false;
            document.getElementById("cvv").required = false;
        }
        else if(selFormaPago.value=="pagoTarjeta"){
            /*
            $("#inputMontoEfectivo").removeAttr('required', false);
            $("#inputNombreApellido").attr('required', true);
            $("#cc").attr('required', true);
            $("#inputFechaVencTarjeta").attr('required', true);
            $("#cvv").attr('required', true);
*/
            document.getElementById("inputMontoEfectivo").required = false;
    
            document.getElementById("inputNombreApellido").required = true;
            document.getElementById("cc").required = true;
            document.getElementById("inputFechaVencTarjeta").required = true;
            document.getElementById("cvv").required = true;
        }
    })
    

    $('#selectTiempoRecepcion').on('change', function(){
        if(selTiempoRec.value=="antesPosible"){
            document.getElementById("inputFechaRecepcion").required = false;
            document.getElementById("inputHoraRecepcion").required = false;
        }
        else if(selTiempoRec.value=="opcionFechaYHora"){
            document.getElementById("inputFechaRecepcion").required = true;
            document.getElementById("inputHoraRecepcion").required = true;
        }
    })
    


});
                